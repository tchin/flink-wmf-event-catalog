package org.wikimedia.eventplatform;

import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.table.catalog.Catalog;
import org.apache.flink.table.factories.CatalogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class WikimediaEventCatalogFactory implements CatalogFactory {

    private static final Logger LOG = LoggerFactory.getLogger(WikimediaEventCatalogFactory.class);

    @Override
    public Catalog createCatalog(Context context) {
//        final FactoryUtil.CatalogFactoryHelper helper =
//            FactoryUtil.createCatalogFactoryHelper(this, context);
//        helper.validate();

        return new WikimediaEventCatalog(
            context.getName(),
            context.getOptions()
        );
    }

    @Override
    public String factoryIdentifier() {
        return "wmfeventcatalog";
    }

    @Override
    public Set<ConfigOption<?>> requiredOptions() {
        final Set<ConfigOption<?>> options = new HashSet<>();
        return options;
    }

    @Override
    public Set<ConfigOption<?>> optionalOptions() {
        Set<ConfigOption<?>> options = new HashSet<>();
        return options;
    }
}
