[![Project Status: Concept <E2><80><93> Minimal or no implementation has been done yet, or the repository is only intended to be a limited example, demo, or proof-of-concept.](https://www.repostatus.org/badges/latest/concept.svg)](https://www.repostatus.org/#concept)

# Getting started
Build with
```bash
./mvnw package -Dcheckstyle.skip -Dforbiddenapis.skip
```

After starting Flink cluster, launch SQL Client with catalog
```
./bin/sql-client.sh -j ../catalog-1.0-SNAPSHOT-jar-with-dependencies.jar
```

Create catalog
```sql
CREATE CATALOG wmfeventcatalog WITH (
	'type' = 'wmfeventcatalog',
	'properties.group.id' = 'catalog-test',
	'scan.startup.mode' = 'latest-offset',
	'json.timestamp-format.standard' = 'ISO-8601',
	'sink-server' = 'eqiad'
);
```

Use catalog
```sql
USE CATALOG wmfeventcatalog;
```

Select (must use backticks)
```sql
SELECT `page_id` FROM `mediawiki.page-create`;
```

Insert (example)
```sql
INSERT INTO `eventgate-main.test.event`
SELECT '/test/event/1.0.0',
   CAST(
        ('', CURRENT_TIMESTAMP, '', '', 'eventgate-main.test.event', '')
        AS
        ROW<`domain` STRING, `dt` TIMESTAMP_LTZ(6), `id` STRING, `request_id` STRING, `stream` STRING, `uri` STRING>
    )
    AS meta,
    'test_from_catalog', MAP['test_key', 'test_val'];
```